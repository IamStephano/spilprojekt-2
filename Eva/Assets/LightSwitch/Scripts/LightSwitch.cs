﻿
using UnityEngine;
using System.Collections;

public class LightSwitch : MonoBehaviour {

	public Light[] lights;
    public AudioClip[] sounds;

	private enum State {
		on,
		off
	}

	private State state;

	void Start () {
		state = LightSwitch.State.off;

	}


	public void OnMouseUp() {
		if (state == LightSwitch.State.on)
						TurnOff ();
				else
						TurnOn ();
	}


	private void TurnOn() {
        if (GetComponent<Animation>() != null)
            GetComponent<Animation>().Play ("TurnOffAnimation");
        GetComponent<AudioSource>().PlayOneShot(sounds[0]);
		state = LightSwitch.State.on;
        foreach (Light item in lights)
        {
            item.enabled = true;
        }
		

}

	private void TurnOff() {
        if(GetComponent<Animation>() != null)
		    GetComponent<Animation>().Play ("TurnOnAnimation");
        GetComponent<AudioSource>().PlayOneShot(sounds[1]);
        state = LightSwitch.State.off;
        foreach (Light item in lights)
        {
            item.enabled = false;
        }

    }

}

