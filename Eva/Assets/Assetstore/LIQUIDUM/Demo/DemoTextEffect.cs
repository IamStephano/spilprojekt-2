using UnityEngine;
using System.Collections;

public class DemoTextEffect : MonoBehaviour {
	
	public float velRot= -0.5f;
	public bool Out=false;

	
		public enum Axis
    {
        X,
        Y,
		Z
    }
	
	public Axis axis = Axis.Y;
	
	void Start () {
    GetComponent<Renderer>().material.color= new Color(0,0,0,0);
	}

	
	
	void Update () {

		
		
		if(axis==Axis.X)
	transform.Rotate(new Vector3(1,0,0),velRot * Time.deltaTime);
				if(axis==Axis.Y)
	transform.Rotate(new Vector3(0,1,0),velRot * Time.deltaTime);
				if(axis==Axis.Z)
	transform.Rotate(new Vector3(0,0,1),velRot * Time.deltaTime);
	if(Out){			
				GetComponent<Renderer>().material.color=Color.Lerp(GetComponent<Renderer>().material.color, new Color(0,0,0,0),0.05f* Time.deltaTime);
	
	}else{			
				GetComponent<Renderer>().material.color=Color.Lerp(GetComponent<Renderer>().material.color, new Color(1,1,1,1),0.5f* Time.deltaTime);
			if(GetComponent<Renderer>().material.color.a>=0.9f)Out=true;
	
	}
}
}