using UnityEngine;
using System.Collections;

public class LiquidumUsage : MonoBehaviour {

	
	Liquidum LiquidumScript;
	public bool showDebugArea;

	//Drag here the event to activate on press E Key
	public Liquidum_AutomaticWeatherEvents myWeatherEvent;
	
	void Start () {
		//Get the liquidum main script
		LiquidumScript=Liquidum.LiquidumScript;
		}
	
	void Update () {
	
		

           ////////////////////////Splash Examples/////////////////////////////////
		
	if(Input.GetKeyDown(KeyCode.Alpha1))//On press Key 1 
			LiquidumScript.SplashNow("Central");//Splash on Center		
		
	if(Input.GetKeyDown(KeyCode.Alpha2))//On press Key 2 
			LiquidumScript.SplashNow("Left");//Splash on Left		
		
		
	if(Input.GetKeyDown(KeyCode.Alpha3))//On press Key 3 
			LiquidumScript.SplashNow("Right");//Splash on Right		
		
		
	if(Input.GetKeyDown(KeyCode.Alpha4))//On press Key 4 
			LiquidumScript.SplashNow("Up");//Splash Up		
		
		
	if(Input.GetKeyDown(KeyCode.Alpha5))//On press Key 5 
			LiquidumScript.SplashNow("Down");//Splash Down	
		
		
		
	if(Input.GetKeyDown(KeyCode.Alpha6))//On press Key 6 
			LiquidumScript.SplashColor=new Color(0f,1f,0,1f);//Change color of Splash to green
		
				
	if(Input.GetKeyDown(KeyCode.Alpha7)){//On press Key 7 
			LiquidumScript.SplashColor=new Color(0.2f,0.2f,0.2f,0.5f);//Change color of Splash to transparent
		    LiquidumScript.SplashDistortion=5;//Change the distortion power
		}
					
	if(Input.GetKeyDown(KeyCode.Alpha8))//On press Key 8 
			LiquidumScript.SplashColor=new Color(1f,0f,0f,1f);//Change color of Splash to red
		
         //////////////////////////End Splash Examples///////////////////////
		
		
		
		
		
		
	
		////////////////////////// Static Examples///////////////////////
							
	if(Input.GetKeyDown(KeyCode.I))//On press Key I 
			LiquidumScript.FadeInStaticNow=true;//Fade in the Static Effect
		
	if(Input.GetKeyDown(KeyCode.O))//On press Key O
			LiquidumScript.FadeOutStaticNow=true;//Dissolve the Static Effect
		
		//////////////////////////End Static Examples///////////////////////
		
		
		
		
		
		
			
		////////////////////////// Additional Distortion Examples///////////////////////
							
	if(Input.GetKeyDown(KeyCode.N))//On press Key I 
			LiquidumScript.FadeInAdditionalDistortionNow=true;//Fade in the  Additional Distortion Effect
		
	if(Input.GetKeyDown(KeyCode.M))//On press Key O
			LiquidumScript.FadeOutAdditionalDistortionNow=true;//Dissolve the  Additional Distortion Effect
		
		//////////////////////////End Additional Distortion Examples///////////////////////
		
		
		
		
		
		
		
		
		////////////////////////// Drops Examples///////////////////////
							
	if(Input.GetKeyDown(KeyCode.C)){//On press Key C
			LiquidumScript.ClearAllDropsImmediately=true;//Fade all the drops on screen
		    LiquidumScript.Emit=false;}//Stop Drops emission
		
	if(Input.GetKeyDown(KeyCode.V)){//On press Key O
			LiquidumScript.DropsColor=new Color(1f,0f,0f,1f);//Change color of Drops to red
		    LiquidumScript.Emit=true;}//Restart Drops emission
		
		
	if(Input.GetKeyDown(KeyCode.B)){//On press Key O
			LiquidumScript.DropsColor=new Color(05f,0.5f,0.5f,0.5f);//Change color of Drops to semi-transparent
		    LiquidumScript.Emit=true;}//Restart Drops emission
			
		//////////////////////////End Splash Examples///////////////////////
		
		
		
		
		////////////////////////Rain Examples//////////////////////////
	if(Input.GetKeyDown(KeyCode.H)){//On press Key H
			Liquidum.LiquidumScript.RainEmit=false;//Stop Rain 
                  }
		
			if(Input.GetKeyDown(KeyCode.J)){//On press Key J
		    LiquidumScript.RainEmit=true;}//Restart Rain emission
		////////////////////////////
		
		
		//////////////////////Thunder Example////////////////////
					if(Input.GetKeyDown(KeyCode.T)){//On press Key T
			LiquidumScript.ThundersDistance=5;//Set away thunder
		    LiquidumScript.ThunderEmitNow=true;}//and emit
		
		
					if(Input.GetKeyDown(KeyCode.Y)){//On press Key Y
			LiquidumScript.ThundersDistance=2;//Set a thunder medium distance
		    LiquidumScript.ThunderEmitNow=true;}//and emit
		
					if(Input.GetKeyDown(KeyCode.U)){//On press Key U
			LiquidumScript.ThundersDistance=0f;//Set a very near thunder distance
		    LiquidumScript.ThunderEmitNow=true;}//and emit
		/////////////////////////////////////////////////////////
		
		
		//////////////////////Weather event Example////////////////////
					if(Input.GetKeyDown(KeyCode.E)){//On press Key E
			LiquidumScript.SetWeatherEvent(myWeatherEvent);//Set the weather
                     }
		/////////////////////////////////////////////////////////
		
	}
	
	
	
	
	
		void OnGUI() {
		if(showDebugArea){
	     GUILayout.BeginArea(new Rect(2, 2, 500, 1000));
	     GUI.color = Color.grey;
   		 
         GUILayout.Label("////LIQUIDUM - Runtime Examples////");
		 GUILayout.Label("Use F/G Key on green buttons for close/open Drains and Cascade");
         GUILayout.Label("Press T Key to Away distance thunder");
	     GUILayout.Label("Press Y Key to Middle distance thunder");
		 GUILayout.Label("Press U Key to Near distance thunder");
		 GUILayout.Label("Press H Key to stop Rain Effect");
		 GUILayout.Label("Press J Key to restart Rain Effect");
         GUILayout.Label("Press 1 to 5 Key to Splash - Central,Left,Right,Up,Down");
		 GUILayout.Label("Press 6 Key to change the splash color to green");
		 GUILayout.Label("Press 7 Key to change the splash color to transparent");
		 GUILayout.Label("Press 8 Key to change the splash color to red");
		 GUILayout.Label("Press I Key to fade-in the Static Effect");
		 GUILayout.Label("Press O Key to fade-out the Static Effect");
		 GUILayout.Label("Press C Key to fade-out all the drops on screen and stop emission");
		 GUILayout.Label("Press V Key to change color of Drops to red");
		 GUILayout.Label("Press B Key to change color of Drops to semi-transparent");		
		 GUILayout.Label("Press N Key to fade-in the  Additional Distortion Effect");
		 GUILayout.Label("Press M Key to dissolve the  Additional Distortion Effect");
         GUILayout.EndArea();
	}
	

	
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
