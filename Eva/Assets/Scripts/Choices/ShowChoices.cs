﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
//using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Serialization;

public class ShowChoices : MonoBehaviour {

    [Header("Sliders")]
    public Slider[] sliders;

    [Header("Stats Text")]
    public Text[] stats;

    [Header("Choice Text")]
    public string[] choiceTextOption1;
    public string[] choiceTextOption2;

    private string path;
    XmlSerializer serializer;

    [SerializeField]
    private Choices board = null;

    string choices;

    IEnumerator Start()
    {
        //Choices to string
        foreach (int choice in Master.choices)
        {
            choices += "," + choice.ToString();
        }
        Debug.Log(choices);
        //Update and Download new statistics from Inkdrop Website
        WWW www = new WWW("http://inkdropgames.com/api/eva/savechoices.php?choices=" + choices);
        while (!www.isDone)
        {
            yield return new WaitForSeconds(0.1f);
        }

        string wwwtext = ASCIIEncoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8, Encoding.ASCII, www.bytes));

        Debug.Log("new xmldoc");
        XmlDocument result = new XmlDocument();

        Debug.Log("loadxml");
        try
        {
            result.LoadXml(wwwtext);
        }
        catch { }

        Debug.Log(wwwtext);
        Debug.Log(result.InnerXml.ToString());

        //Serialize XML result
        serializer = new XmlSerializer(typeof(Choices));
        string xml = result.InnerXml.ToString();
        using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(xml)))
        {
            board = (Choices)serializer.Deserialize(stream);
        }

        //Display Result
        for (int i = 0; i < stats.Length; i++)
        {
            if (Master.choices[i] == 0)
            {
                stats[i].text = "Dig og " + CalcPercent(board.Choice[i].Option[0], board.Choice[i].Option[1]) + "% " + choiceTextOption1[i];
                sliders[i].value = CalcPercent(board.Choice[i].Option[0], board.Choice[i].Option[1]);
            }
            else
            {
                stats[i].text = "Dig og " + CalcPercent(board.Choice[i].Option[1], board.Choice[i].Option[0]) + "% " + choiceTextOption2[i];
                sliders[i].value = CalcPercent(board.Choice[i].Option[1], board.Choice[i].Option[0]);
            }
        }
    }

    int CalcPercent (float choice, float other)
    {
        float result = choice / (choice + other) * 100f;
        return (int)result;
    }
}
