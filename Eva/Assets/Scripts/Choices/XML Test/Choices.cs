﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

[Serializable]
[XmlRoot(ElementName = "choice")]
public class Choice
{
    [XmlElement(ElementName = "option")]
    public List<int> Option = new List<int>();
}

[Serializable]
[XmlRoot(ElementName = "choices")]
public class Choices
{
    [XmlElement(ElementName = "choice")]
    public List<Choice> Choice = new List<Choice>();
}




