﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Diagnostics;
using Debug = UnityEngine.Debug;

public enum Item
{
	phone, homework, selfharm_haircolor, beer, medicin
}

public class MakeChoice : MonoBehaviour {

    [Header("Item")]
    public Item item;
    public Renderer mesh;

	[Header("Phone Settings")]
    public int state = 0;
    public Material standardTex;
    public Material engagedTex2;
    public Material engagedTex;
	public AudioClip vibrationSound;
	


    public bool choiceMade = false;
    public bool choicesShown;

	[Header("UI Settings")]
	public GameObject ActionPanel;
    public Text choiceTextOne;
	public Text choiceTextTwo;

    public string option1;
    public string option2;

    public void ShowChoices ()
    {
		ActionPanel.SetActive(true);
	    choiceTextOne.text = option1;
		choiceTextTwo.text = option2;
	    choicesShown = true;
	    //InvokeRepeating("Select", .5f, .1f);
    }

    public void HideChoices ()
    {
		ActionPanel.SetActive(false);
		CancelInvoke();
        choicesShown = false;
    }

    void Update ()
    {
        if(!choiceMade && choicesShown)
        {
            if (Input.GetKeyDown(KeyCode.V) || Input.GetKeyDown(KeyCode.JoystickButton3))
            {
                Debug.Log("CHOICE MADE!");
                SaveChoice(0);
                switch (item)
                {
                    case Item.phone:
                        StartCoroutine("PhoneSequence");
                        break;
					case Item.medicin:
						Debug.Log("good end start");
						GoodEnd();
						//Invoke("GoodEnd", 2f);
		                break;
                }
				choiceMade = true;
				HideChoices();
			}
            else if (Input.GetKeyDown(KeyCode.B) || Input.GetKeyDown(KeyCode.JoystickButton1))
            {
                
                SaveChoice(0);
                switch (item)
                {
                    case Item.phone:
                    //    PhoneSequence(false);
                        break;
					case Item.medicin:
						//Invoke("BadEnd", 2f);
						BadEnd();
						break;
				}
				choiceMade = true;
				HideChoices();

			}
            
        } 
    }

    IEnumerator PhoneSequence()
    {
        Debug.Log("Changing Texture");
		GetComponent<AudioSource>().PlayOneShot(vibrationSound);
        Material[] mats = mesh.materials;
        mats[1] = engagedTex;
        mats[0] = mesh.materials[0];
        mesh.materials = mats;
		yield return new WaitForSeconds(2);
		mats[1] = engagedTex2;
		mats[0] = mesh.materials[0];
		mesh.materials = mats;
	}

    void SaveChoice(int choice)
    {
        Debug.Log("choice made");
        switch (item)
        {
            case Item.homework:
                Master.choices[0] = choice;
                break;
            case Item.phone:
                Master.choices[1] = choice;
                break;
            case Item.selfharm_haircolor:
                Master.choices[2] = choice;
                break;
            case Item.beer:
                Master.choices[3] = choice;
                break;
            case Item.medicin:
                Master.choices[4] = choice;
                break;
            
        }
    }

	void GoodEnd()
	{
		Debug.Log("good end");
		Application.LoadLevel(4);
	}

	void BadEnd()
	{
		Application.LoadLevel(5);
	}


}
