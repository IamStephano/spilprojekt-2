﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class Progression : MonoBehaviour {

    [Header("Materials")]
    public Material standardMaterial;
    public Material hightlighedMaterial;

    void OnMouseEnter()
    {
        transform.GetComponent<Renderer>().material = hightlighedMaterial;
        GameObject.Find("Action Text").GetComponent<Text>().text = "Fortsæt til næste sekvens";
    }

    void OnMouseExit()
    {
        transform.GetComponent<Renderer>().material = standardMaterial;
        GameObject.Find("Action Text").GetComponent<Text>().text = "";
    }

    void OnMouseOver()
    {
        if (Input.GetButtonDown("Fire1"))
            Application.LoadLevel(Application.loadedLevel + 1);
        //SceneManager.LoadScene()



    }
}
