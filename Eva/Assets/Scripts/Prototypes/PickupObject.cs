﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PickupObject : MonoBehaviour
{
    [Header("Setup")]
	public string name;
    public string text;
    public bool keyObject;
    public bool choiceObject;
    public AudioClip narration;

    public bool narrationPlayed;

    void OnMouseEnter()
	{
		GetComponentInChildren<Renderer>().material.shader = Shader.Find("Toon/Lit Outline");
		GetComponentInChildren<Renderer>().material.SetColor("_OutlineColor", Color.white);
		GameObject.Find("Action Text").GetComponent<Text>().text = "Tag " + name;
	}

	void OnMouseExit()
	{
		GetComponentInChildren<Renderer>().material.shader = Shader.Find("Standard");
		GameObject.Find("Action Text").GetComponent<Text>().text = "";
	}
}
