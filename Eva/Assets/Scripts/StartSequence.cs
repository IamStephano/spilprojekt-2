﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

public class StartSequence : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
	    GameObject.Find("Action Text").GetComponent<Text>().text = "Tryk på en vilkårlig tast for at rejse dig op";
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.anyKey)
        {
            GetComponent<Animation>().Play();
            GetComponent<FirstPersonController>().enabled = true;
            GameObject.Find("Action Text").GetComponent<Text>().text = "";
            Destroy(this);
        }
	
	}
}
