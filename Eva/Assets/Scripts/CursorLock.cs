﻿using UnityEngine;
using System.Collections;

public class CursorLock : MonoBehaviour {
    private bool lockCursor;

    void Start()
    {
        lockCursor = true;
    }

    void OnGUI()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            lockCursor = false;

        if (Input.GetMouseButtonDown(0) && !lockCursor)
            lockCursor = true;

        if (lockCursor)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = true;
        }
        else {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}
