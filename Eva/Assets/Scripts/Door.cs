﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Door : MonoBehaviour
{
    public AudioClip sound;

	[Header("Rotation Settings")]
	public Vector3 openRotation;
	public Vector3 closeRotation;

	[Header("State")]
	public bool open = false;

	[Header("Speed Settings")]
	[Range(10,100)]public float speed = 50;

	[Header("Materials")]
	public Material standardMaterial;
	public Material hightlighedMaterial;

	[Header("Messages")]
	public string openText;

	public string closedText;

	// Update is called once per frame
	void Update () {
		if (open)
		{
			float step = speed*Time.deltaTime;
			transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(openRotation), step);
		}
		if (!open)
		{
			float step = speed * Time.deltaTime;
			transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(closeRotation), step);
		}
	}

	void OnMouseEnter()
	{
		transform.GetChild(0).GetComponent<Renderer>().material = hightlighedMaterial;
		GameObject.Find("Action Text").GetComponent<Text>().text = open ? closedText : openText;
	}

	void OnMouseExit()
	{
		transform.GetChild(0).GetComponent<Renderer>().material = standardMaterial;
		GameObject.Find("Action Text").GetComponent<Text>().text = "";
	}

	void OnMouseOver()
	{
        if (Input.GetButtonDown("Fire1"))
        {
            open = !open;
            GetComponent<AudioSource>().PlayOneShot(sound);
        }

		GameObject.Find("Action Text").GetComponent<Text>().text = open ? closedText : openText;
	}
}
