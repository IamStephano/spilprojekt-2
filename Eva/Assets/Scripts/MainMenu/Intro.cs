﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Intro : MonoBehaviour
{
#if !UNITY_WEBGL
    public MovieTexture movTexture;
#endif

    void Start()
    {
#if !UNITY_WEBGL
        GetComponent<RawImage>().material.mainTexture = movTexture;
        movTexture.Play();
#else 
        SceneManager.LoadScene(1);
#endif

    }

#if !UNITY_WEBGL
    void Update()
    {
        if (!movTexture.isPlaying)
        {
            Debug.Log("movie end");
            SceneManager.LoadScene(1);
        }
    }
#endif

}
