﻿using UnityEngine;
using System.Collections;

public class RotationBug : MonoBehaviour
{

	// Use this for initialization
	private void Start()
	{

	}

	// Update is called once per frame
	private void LateUpdate()
	{
		if(transform.parent.GetComponent<Rigidbody>().useGravity)
			transform.rotation = transform.parent.rotation;
	}
}
