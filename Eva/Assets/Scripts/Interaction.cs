﻿using UnityEngine;
using UnityEngine.UI;

public class Interaction : MonoBehaviour
{

	public string unactivated;
	public string actived;

	public string type;

	public Material onMaterial;
	public Material offMaterial;

	public bool isOn;

    public AudioClip interactionSound;

	void OnMouseEnter()
	{
		GetComponentInChildren<Renderer>().material.shader = Shader.Find("Toon/Lit Outline");
		GetComponentInChildren<Renderer>().material.SetColor("_OutlineColor", Color.white);
		GameObject.Find("Action Text").GetComponent<Text>().text = isOn ? actived : unactivated;
	}

	void OnMouseExit()
	{
		GetComponentInChildren<Renderer>().material.shader = Shader.Find("Standard");
		GameObject.Find("Action Text").GetComponent<Text>().text = "";
	}

	void OnMouseOver()
	{
		GameObject.Find("Action Text").GetComponent<Text>().text = isOn ? actived : unactivated;
		if (Input.GetButtonDown("Fire1"))
		{
            if (interactionSound != null)
                GetComponent<AudioSource>().PlayOneShot(interactionSound);

			if (type == "TV")
			{
				Material[] mats = GetComponent<Renderer>().materials;
				mats[0] = GetComponent<Renderer>().materials[0];
				mats[1] = isOn ? offMaterial : onMaterial;
				//mats[2] = GetComponent<Renderer>().materials[2];
				//mats[3] = GetComponent<Renderer>().materials[3];
				GetComponent<Renderer>().materials = mats;
				isOn = !isOn;
			}
			else if (type == "Flatscreen")
			{
				Material[] mats = GetComponent<Renderer>().materials;
				mats[0] = GetComponent<Renderer>().materials[0];
				mats[1] = isOn ? offMaterial : onMaterial;
				//mats[2] = GetComponent<Renderer>().materials[2];
				//mats[3] = GetComponent<Renderer>().materials[3];
				GetComponent<Renderer>().materials = mats;
				isOn = !isOn;
			}
            else
            {
                isOn = !isOn;
            }
		}

	}
}
