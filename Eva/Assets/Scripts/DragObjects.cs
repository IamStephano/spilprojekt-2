﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.ImageEffects;

public class DragObjects : MonoBehaviour
{
	private Vector3 offset;
	private float _lockedYPosition;
	private Vector3 screenPoint;
	private GameObject Player;
    private float zoom = .35f;
    private GameObject target;

    private Transform RotationFix;
    private Quaternion lastRot;

    private DisplayManager displayManager;

    private bool targetIsKey = false;

    [Header("Audio Settings")]
    public AudioClip sound;
    public AudioClip[] narration;
    public bool[] played;

    [Header("Setup")]
    public Camera camera;

    [Header("State")]
    public bool holdingObject;

    void Start()
    {
        displayManager = (DisplayManager)GetComponent(typeof(DisplayManager));
    }

	void Update()
	{
		RaycastHit hit;
		//Ray ray = camera.ScreenPointToRay(Input.mousePosition);
		Debug.DrawRay(transform.position, transform.forward * 4f, Color.green);

		//Debug.Log(Input.GetAxis("Trigger_L"));
		//Debug.Log(Input.GetAxis("Trigger"));

		if (Input.GetButtonDown("Fire1"))
		{
			if (!holdingObject)
			{
				if (Physics.Raycast(transform.position, transform.forward * 4f, out hit))
				{
					//Debug.Log(hit.collider.tag);
					if (hit.collider.CompareTag("Dragable"))
					{
						target = hit.collider.gameObject;
						target.GetComponent<Rigidbody>().freezeRotation = true;
						target.GetComponent<Rigidbody>().useGravity = false;
						target.GetComponent<BoxCollider>().enabled = false;
						Debug.Log(target.transform.GetChild(0).gameObject.layer);
						if (target.transform.GetChild(0).gameObject.layer != 8)
						{
							targetIsKey = false;
							SetLayerRecursively(target, 8);
                            Debug.Log("Target is not key, changing layers...");
						}
						else
						{
							targetIsKey = true;
							Debug.Log("Target is key!");
						}
						target.layer = 8;
						//Camera.main.transform.GetComponent<Blur>().enabled = true;
						GetComponent<AudioSource>().PlayOneShot(sound);

                        if (target.GetComponent<PickupObject>().keyObject && !target.GetComponent<PickupObject>().narrationPlayed)
                        {
                            PlayNarration(target.GetComponent<PickupObject>().narration, target.GetComponent<PickupObject>().text);
                            target.GetComponent<PickupObject>().narrationPlayed = true;
                        }

                        if (target.GetComponent<PickupObject>().choiceObject)
                        {
							Debug.Log("showing choices");
                            if (!target.GetComponent<MakeChoice>().choiceMade)
                                target.GetComponent<MakeChoice>().ShowChoices();

							if (target.GetComponent<MakeChoice>().item == Item.phone)
							{
								target.GetComponent<Animator>().enabled = false;
								target.GetComponent<AudioSource>().loop = false;
							}
						}

						

                        holdingObject = true;
					}
				}
			}
			else
			{
				//Camera.main.transform.GetComponent<Blur>().enabled = false;
				target.GetComponent<Rigidbody>().freezeRotation = false;
				target.GetComponent<Rigidbody>().useGravity = true;
				if (!targetIsKey)
					SetLayerRecursively(target, 0);

				target.GetComponent<BoxCollider>().enabled = true;
				//target.transform.GetChild(0).LookAt(transform.position);
				//if (target.name == "Comic Book")
				zoom = .35f;
                if (target.GetComponent<PickupObject>().choiceObject)
                {
                    //if (!target.GetComponent<MakeChoice>().choiceMade)
                        target.GetComponent<MakeChoice>().HideChoices();
                }

				if (target.GetComponent<PickupObject>().choiceObject)
				{
					if (!target.GetComponent<MakeChoice>().choiceMade)
					{
						if (target.GetComponent<MakeChoice>().item == Item.phone)
						{
							target.GetComponent<Animator>().enabled = true;
							target.GetComponent<AudioSource>().loop = true;
						}
					}
				}

				holdingObject = false;
			}
		}
		if (holdingObject)
		{
			//zoom = Mathf.Clamp(zoom + Input.GetAxis("Mouse ScrollWheel")/5 + Input.GetAxis("Trigger")/40, .25f, 2f);
			//target.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, zoom));
			target.transform.position = transform.position + transform.forward*zoom;

			target.transform.LookAt(transform.position);

			var targetRot = target.transform.GetChild(0).rotation;

			if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.JoystickButton4))
				target.transform.GetChild(0).rotation *= Quaternion.AngleAxis(3, Vector3.up);

			if (Input.GetKey(KeyCode.E) || Input.GetKey(KeyCode.JoystickButton5))
				target.transform.GetChild(0).rotation *= Quaternion.AngleAxis(3, Vector3.down);
		}
	}

	void SetLayerRecursively(GameObject obj, int newLayer)
	{
		if (null == obj)
			return;

		obj.layer = newLayer;

		foreach (Transform child in obj.transform)
		{
			if (null == child)
				continue;
			
			SetLayerRecursively(child.gameObject, newLayer);
		}
	}

    void PlayNarration(AudioClip narration, string text)
    {
        GetComponent<AudioSource>().PlayOneShot(narration);
        displayManager.DisplayMessage(text);
    }
}