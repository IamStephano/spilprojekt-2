﻿using System.Timers;
using UnityEngine;
using UnityEngine.UI;

public class Drawer : MonoBehaviour
{
	public AudioClip sound;

	[Header("Positions")]
	public Vector3 startPosition;
	public Vector3 openPosition;
	public Vector3 closedPosition;

	[Header("Rotations")] 
	public Vector3 unactivatedRotation;
	public Vector3 activatedRotation;
	
	[Header("State")]
	public bool open = false;
	public bool activated = false;

	[Header("Speed Settings")]
	[Range(0, 1)]public float speed = .5f;

	[Header("Materials")]
	public Material standardMaterial;
	public Material hightlighedMaterial;

	private new AudioSource _audio;
	private Text _actionText;


	void Start()
	{
		startPosition = transform.position;
		unactivatedRotation = transform.rotation.eulerAngles;
		_audio = (AudioSource) GetComponent(typeof (AudioSource));
		_actionText = (Text)GameObject.Find("Action Text").GetComponent(typeof (Text));
	}
	void Update () {
		if (activated)
		{
			if (open)
			{
				float step = speed * Time.deltaTime;
				transform.position = Vector3.MoveTowards(transform.position, openPosition, step);
				transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(activatedRotation), 50*Time.deltaTime);
			}
			if (!open)
			{
				float step = speed*Time.deltaTime;
				transform.position = Vector3.MoveTowards(transform.position, closedPosition, step);
			}
		}
	}

	void OnMouseEnter()
	{
		GetComponent<Renderer>().material = hightlighedMaterial;
		GameObject.Find("Action Text").GetComponent<Text>().text = open ? "LUK SKUFFE" : "ÅBEN SKUFFE";
	}

	void OnMouseExit()
	{
		GetComponent<Renderer>().material = standardMaterial;
		_actionText.text = "";
	}

	void OnMouseOver()
	{
		_actionText.text = open ? "LUK SKUFFE" : "ÅBEN SKUFFE";

		if (Input.GetButtonDown("Fire1"))
		{
			if (!activated)
				activated = true;

			open = !open;
			_audio.Stop();
			_audio.PlayOneShot(sound);

		}
		
	}
}
