﻿using UnityEngine;
using System.Collections;

public class DoorBehaviour : MonoBehaviour
{
	private bool closeDoor = false;
	private bool openDoor = false;



	private void Update()
	{
		RaycastHit hit;
		Debug.DrawRay(transform.position, transform.forward*1.5f, Color.green);

		if (Physics.Raycast(transform.position, transform.forward*1.5f, out hit))
		{
			Debug.Log(hit.collider.tag);
			if (Input.GetButtonDown("Fire1"))
			{
				//Debug.Log("Fire");
				if (hit.collider.CompareTag("DoorL"))
				{
					hit.collider.GetComponent<Door>().open = !hit.collider.GetComponent<Door>().open;
				}
				else if (hit.collider.CompareTag("Drawer"))
				{
					hit.collider.GetComponent<Drawer>().open = !hit.collider.GetComponent<Drawer>().open;
				}
			}
		}
	}



}
