﻿using UnityEngine;
using System.Collections;
using UnityEngine.VR;

public class VR : MonoBehaviour
{

	void Start()
	{
		UnityEngine.VR.InputTracking.Recenter();
	}

	void Update()
	{
		if (Input.GetKey(KeyCode.R))
		{
			UnityEngine.VR.InputTracking.Recenter();
		}
	}
}