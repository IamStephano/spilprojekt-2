﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadStats : MonoBehaviour
{

	// Use this for initialization


	// Update is called once per frame
	//void LoadStatistics () {
	//	Application.LoadLevel(6);
	//}

	//IEnumerator LoadStatistics()
	//{
	//	yield return new WaitForSeconds(22);
	//	Application.LoadLevel(6);

	//}

	public float fadeSpeed = 1.5f;          // Speed that the screen fades to and from black.


	private bool sceneStarting = true;      // Whether or not the scene is still fading in.
	public Image image;
	public Image image2;
	public AudioClip audio;
	public AudioSource music;

	void Update()
	{
		// If the scene is starting...
		if (sceneStarting)
			// ... call the StartScene function.
			StartScene();
	}

	void Awake()
	{
		//image = GetComponent<Image>();
		Invoke("EndStat", 24);
	}


	void FadeToClear()
	{
		// Lerp the colour of the texture between itself and transparent.
		Debug.Log("Fadding To clear");
		image.color = Color.Lerp(image.color, Color.clear, fadeSpeed * Time.deltaTime);
	}


	void FadeToBlack()
	{
		// Lerp the colour of the texture between itself and black.
		image.color = Color.Lerp(image.color, Color.black, fadeSpeed * Time.deltaTime);
	}

	void FadeToBlack2()
	{
		// Lerp the colour of the texture between itself and black.
		image2.color = Color.Lerp(image.color, Color.black, fadeSpeed * Time.deltaTime);
	}


	void StartScene()
	{
		// Fade the texture to clear.
		FadeToClear();

		// If the texture is almost clear...
		if (image.color.a <= 0.05f)
		{
			// ... set the colour to clear and disable the GUITexture.
			image.color = Color.clear;
			//image.enabled = false;

			// The scene is no longer starting.
			sceneStarting = false;
		}
	}

	public void EndStat()
	{
		InvokeRepeating("FadeToBlack",0,Time.deltaTime);//FadeToBlack();
		music.volume = 0;
		GetComponent<AudioSource>().PlayOneShot(audio);
		image.enabled = true;
		Invoke("EndScene", 10);
	}

	public void EndScene()
	{
		// Make sure the texture is enabled.
		image.enabled = true;

		// Start fading towards black.
		FadeToBlack();

		// If the screen is almost black...
		if (image.color.a >= 0.95f)
			// ... reload the level.
			Application.LoadLevel(6);
	}
}
